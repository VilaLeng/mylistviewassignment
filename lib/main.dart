import 'dart:ui';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  //const MyApp({Key? key}) : super(key: key);

  final titles = [
    'Bike',
    'Boat',
    'Bus',
    'Car',
    'Railway',
    'Run',
    'Subway',
    'Transit',
    'Walk'
  ];
  final icons = [
    Icons.directions_bike,
    Icons.directions_boat,
    Icons.directions_bus,
    Icons.directions_car,
    Icons.directions_railway,
    Icons.directions_run,
    Icons.directions_subway,
    Icons.directions_transit,
    Icons.directions_walk,
  ];
  final images = [
    AssetImage("assets/images/bike.jpg"),
    AssetImage("assets/images/boat.jpg"),
    AssetImage("assets/images/bus.jpg"),
    AssetImage("assets/images/car.jpg"),
    AssetImage("assets/images/railway.jpg"),
    AssetImage("assets/images/run.jpg"),
    AssetImage("assets/images/subway.jpg"),
    AssetImage("assets/images/transit.jpg"),
    AssetImage("assets/images/walking.jpg"),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Basic List View',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            'The Basic List View of 0030.',
            style: TextStyle(
                color: Colors.blueGrey[100], fontWeight: FontWeight.bold),
          ),
        ),
        body: ListView.builder(itemBuilder: (BuildContext context, int index) {
          return Column(
            children: [
              ListTile(
                // leading: Icon(
                //   icons[index],
                //   size: 25,
                // ),
                leading: CircleAvatar(
                  backgroundImage: images[index],
                  radius: 30,
                ),
                title: Text(
                  '${titles[index]}',
                  style: TextStyle(fontSize: 18, color: Colors.blue),
                ),
                subtitle: Text(
                  'There is a ${titles[index]} in city today.',
                  style: TextStyle(fontSize: 15),
                ),
                trailing: Icon(
                  //Icons.notifications_none,
                  icons[index],
                  size: 25,
                  color: Colors.blue,
                ),
                onTap: () {
                  AlertDialog alert = AlertDialog(
                    backgroundColor: Colors.blueGrey[100],
                    title: Text(
                      'Welcome',
                      style: TextStyle(
                          color: Colors.blueGrey, fontWeight: FontWeight.bold),
                    ),
                    content: Text(
                      'This is ${titles[index]}',
                      style: TextStyle(
                          color: Colors.blueGrey, fontWeight: FontWeight.bold),
                    ),
                    actions: [
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          'CANCEL',
                          style: TextStyle(
                              color: Colors.blueGrey[100],
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          'ACCEPT',
                          style: TextStyle(
                              color: Colors.blueGrey[100],
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  );
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return alert;
                    },
                  );
                },
              ),
              Divider(
                thickness: 0.8,
                color: Colors.blue,
              ),
            ],
          );
        }),
        backgroundColor: Colors.blueGrey[100],
      ),
    );
  }
}
